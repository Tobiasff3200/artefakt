from django.apps import AppConfig


class Pm2SimConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'pm2sim'
