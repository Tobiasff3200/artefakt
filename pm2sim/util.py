#   Copyright © 2024 Tobias Mieves
#  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#  THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import datetime
import logging
import math
import pathlib
import uuid

import numpy as np
import pandas as pd
import pm4py
import simpy
from django.core.exceptions import ObjectDoesNotExist
from pandas import DataFrame, Series
from pm4py import Marking, PetriNet
from pm4py.objects.log.obj import EventLog
from pm4py.objects.petri_net import semantics
from pm4py.objects.powl.obj import Transition
from pm4py.visualization.bpmn import visualizer as bpmn_visualizer
from tqdm import tqdm

from pm2sim.models import (
    EventChangeStat,
    EventChangeStatMetric,
    EventTime,
    SimulationRun,
)

logger = logging.getLogger(__name__)


def add_to_stats(stats, key: str, value: int) -> None:
    if key not in stats:
        stats[key] = [value]
    else:
        stats[key].append(value)


def calculate_and_add_time(events, timetaken) -> None:
    """
    Calculate the duration and add to timetaken
    :param events:
    :type events:
    :param timetaken:
    :type timetaken:
    """
    events = events.sort_values(by="time:timestamp", ascending=True)
    for i, name in enumerate(events["concept:name"]):
        next_event = None
        if i < (len(events) - 1):
            next_event = events.iloc[i + 1]
        event = events.loc[events["concept:name"] == name].iloc[0]
        if "time:complete" in event:
            add_to_stats(
                timetaken,
                name,
                (event["time:complete"] - event["time:timestamp"]).total_seconds(),
            )
        else:
            if next_event is not None:
                if "time:timestamp" in event:
                    time = event["time:timestamp"]
                    if "time:timestamp" in next_event:
                        next_time = next_event["time:timestamp"]
                    else:
                        next_time = time
                    add_to_stats(timetaken, name, (next_time - time).total_seconds())
            else:
                add_to_stats(timetaken, name, 0)


def get_concept_times(event_log: EventLog | DataFrame) -> DataFrame:
    """
    Calculates the duration of the activities
    :param event_log: the event log to analyse
    :type event_log: EventLog | DataFrame
    :return: Duration of every event
    :rtype: DataFrame
    """
    logger.info("Calculating mean time for every concept")
    timetaken = {}
    for case in tqdm(event_log["case:concept:name"].unique()):
        variants: DataFrame = event_log.loc[event_log["case:concept:name"] == case]
        if "case:variant" in variants:
            for variant in variants["case:variant"].unique():
                events: DataFrame = event_log.loc[
                    (event_log["case:concept:name"] == case)
                    & (event_log["case:variant"] == variant)
                ]
                calculate_and_add_time(events, timetaken)
        else:
            events: DataFrame = event_log.loc[(event_log["case:concept:name"] == case)]
            calculate_and_add_time(events, timetaken)
    df = pd.DataFrame.from_dict(timetaken, orient="index")
    return df.transpose()


def save_time_to_db(
    timetaken: DataFrame, sim_run: SimulationRun, simulated: bool
) -> None:
    """
    Create new EventTime for every Activity
    :param timetaken: The durations od the activities
    :type timetaken: DataFrame
    :param sim_run: The assosiated simulation run
    :type sim_run: SimulationRun
    :param simulated: Indicates if the data is simulated or from the original event log
    :type simulated: bool
    """
    for key in timetaken.keys():
        try:
            event = EventTime.objects.get(simulation_run=sim_run, name=key)
            if simulated:
                event.mean_time_s = np.mean(timetaken[key])
                event.std_deviation_s = np.std(timetaken[key])
            else:
                event.mean_time_o = np.mean(timetaken[key])
                event.std_deviation_o = np.std(timetaken[key])
            event.save()
        except ObjectDoesNotExist:
            if simulated:
                event = EventTime(
                    name=key,
                    mean_time_s=np.mean(timetaken[key]),
                    std_deviation_s=np.std(timetaken[key]),
                    simulation_run=sim_run,
                )
                event.save()
            else:
                event = EventTime(
                    name=key,
                    mean_time_o=np.mean(timetaken[key]),
                    std_deviation_o=np.std(timetaken[key]),
                    simulation_run=sim_run,
                )
                event.save()


def get_process_start_time_stats(event_log: EventLog | DataFrame) -> (float, float):
    """
    Calculates the mean and std of the timeout between the processes
    :param event_log: The event log to analyse
    :type event_log: EventLog | DataFrame
    :return: Tuple(mean_time, std_time)
    :rtype: (float, float)
    """
    logger.info("Calculating start time for every process")
    start_times = []
    for case in tqdm(event_log["case:concept:name"].unique()):
        variants: DataFrame = event_log.loc[event_log["case:concept:name"] == case]
        if "case:variant" in variants:
            for variant in variants["case:variant"].unique():
                events: DataFrame = event_log.loc[
                    (event_log["case:concept:name"] == case)
                    & (event_log["case:variant"] == variant)
                ]
                events = events.sort_values(by="time:timestamp", ascending=True)
                start_times.append(events["time:timestamp"].iloc[0])
        else:
            events: DataFrame = event_log.loc[(event_log["case:concept:name"] == case)]
            events = events.sort_values(by="time:timestamp", ascending=True)
            start_times.append((events["time:timestamp"].iloc[0]))
    start_times.sort()
    intervals = []
    for i in range(len(start_times) - 1):
        intervals.append((start_times[i + 1] - start_times[i]).total_seconds())
    logger.info("Finished calculating start time for every process")
    return np.mean(intervals), np.std(intervals)


def get_event_change_stats(event_log: EventLog | DataFrame):
    """
    Calculates the even change probabilities
    :param event_log: The event Log to analyse
    :type event_log: EventLog | DataFrame
    :return:
    :rtype:
    """
    logger.info("Starting to calculate statistics on event change")
    stats = {}
    for case in tqdm(event_log["case:concept:name"].unique()):
        variants: DataFrame = event_log.loc[event_log["case:concept:name"] == case]
        if "case:variant" in variants:
            for variant in variants["case:variant"].unique():
                events: DataFrame = event_log.loc[
                    (event_log["case:concept:name"] == case)
                    & (event_log["case:variant"] == variant)
                ]
                events = events.sort_values(by="time:timestamp", ascending=True)
                calculate_and_add_follow_events(events, stats)
        else:
            events: DataFrame = event_log.loc[(event_log["case:concept:name"] == case)]
            events = events.sort_values(by="time:timestamp", ascending=True)
            calculate_and_add_follow_events(events, stats)
    # normalize values
    for key in stats.keys():
        sum_all: float = np.sum(np.fromiter(stats[key].values(), dtype=int))
        for ev in stats[key].keys():
            stats[key][ev] = (stats[key][ev]) / (sum_all)
    logger.info("Finished calculat statistics on event change")
    return stats


def calculate_and_add_follow_events(events: DataFrame, stats: dict) -> None:
    """
    Adds the next event to the list of following events
    :param events: following events
    :type events: DataFrame
    :param stats: statistics
    :type stats: dict
    """
    for i in range(len(events) - 1):
        event = events["concept:name"].iloc[i]
        next_event = events["concept:name"].iloc[i + 1]
        if event not in stats.keys():
            stats[event] = {}

        if next_event in stats[event].keys():
            stats[event][next_event] += 1
        else:
            stats[event][next_event] = 1


def get_next_event_weighted(stats: dict, curr_event: str) -> str:
    """
    Choose the next event randomly weighted by the event change statistics
    :param stats: The event change statistics
    :type stats: dict
    :param curr_event: The current event
    :type curr_event: str
    :return:
    :rtype:
    """
    return np.random.choice(
        np.fromiter(stats[curr_event].keys(), dtype="U100"),
        p=np.fromiter(stats[curr_event].values(), dtype="float"),
    )


def generate_bpmn_process_img(event_log: EventLog | DataFrame, id: uuid) -> str:
    """
    Generates the bpmn process image
    :param event_log: The event log to visualise
    :type event_log: EventLog | DataFrame
    :param id: ID of the event log
    :type id: uuid
    :return: Path to the generated image
    :rtype: str
    """
    logger.info("Generating bpmn process image")
    logger.debug("Discover process tree")
    process_tree = pm4py.discover_process_tree_inductive(event_log)
    logger.debug("Convert to bpmn")
    bpmn_model = pm4py.convert_to_bpmn(process_tree)

    parameters = bpmn_visualizer.Variants.CLASSIC.value.Parameters
    gviz = bpmn_visualizer.apply(
        bpmn_model,
        parameters={parameters.FORMAT: "png", "bgcolor": "white", "rankdir": "LR"},
    )
    tmp_path = f"/tmp/{id}"

    gviz.render(tmp_path)
    return tmp_path + ".png"


def read_event_log(
    path: str, case_id="case_id", activity_key="activity", timestamp_key="timestamp"
) -> DataFrame | EventLog:
    """
    Reads event log from a csv or xes file
    :param path: Path to the file to read
    :type path: str
    :param case_id: Name of the case id column
    :type case_id: str
    :param activity_key: Name of the activity key column
    :type activity_key: str
    :param timestamp_key: Name of the timestamp column
    :type timestamp_key: str
    :return: The Event Log read from the file
    :rtype: EventLog | DataFrame
    """
    logger.info("Read Event Log")
    event_log = None
    file_extension = pathlib.Path(path).suffix
    if file_extension == ".xes":
        logger.debug("Start reading xes")
        event_log = pm4py.read_xes(path)
    elif file_extension == ".csv":
        logger.debug("Start reading csv")
        df = pd.read_csv(path)
        event_log = pm4py.format_dataframe(
            df, case_id=case_id, activity_key=activity_key, timestamp_key=timestamp_key
        )
    logger.debug("Finished reading")
    return event_log


class EventLogDataFrame:
    def __init__(self):
        self.event_log = DataFrame(
            {
                "concept:name": Series(dtype="str"),
                "case:concept:name": Series(dtype="str"),
                "time:timestamp": Series(dtype="datetime64[s]"),
                "time:complete": Series(dtype="datetime64[s]"),
            }
        )

    def add(self, row: Series):
        self.event_log = pd.concat(
            [self.event_log, row.to_frame().T],
            ignore_index=True,
        )


def generate_simulation_processes(
    env: simpy.Environment,
    net: PetriNet,
    start_markings: Marking,
    timetaken,
    start_times,
    event_change_stats,
    evLogDF: EventLogDataFrame,
    starttime: datetime,
    num_cases: int = 100,
):
    """
    Generates new simulation processes
    :param env: Simulation environment
    :type env: simpy.Environment
    :param net: The petri net to calculate
    :type net: PetriNet
    :param start_markings: The start markings of the petri net
    :type start_markings: Marking
    :param timetaken:
    :type timetaken:
    :param start_times:
    :type start_times:
    :param event_change_stats:
    :type event_change_stats:
    :param evLogDF:
    :type evLogDF:
    :param starttime:
    :type starttime:
    :param num_cases: Number of cases to generate
    :type num_cases: int
    """
    for i in range(num_cases):
        timeout = min(
            max(np.random.normal(start_times[0], start_times[1]), 0), 2 * start_times[0]
        )
        env.process(
            simulate_net(
                env,
                net,
                start_markings,
                timetaken,
                event_change_stats,
                i,
                evLogDF,
                starttime,
            )
        )
        yield env.timeout(timeout)


def simulate_net(
    env: simpy.Environment,
    net: PetriNet,
    markings: Marking,
    timetaken: DataFrame,
    event_change_stats,
    id: int,
    eLogDF,
    start_time: pd.Timestamp,
):
    """
    Generates a simulated process and saves the events to the log
    :param env: The simulation environment
    :type env: simpy.Environment
    :param net: The petri net
    :type net: PetriNet
    :param markings: The start markings
    :type markings: Marking
    :param timetaken:
    :type timetaken:
    :param event_change_stats:
    :type event_change_stats:
    :param id: The case id
    :type id: int
    :param eLogDF:
    :type eLogDF:
    :param start_time:
    :type start_time:
    """
    # Calculate mean times and std per event
    attributes = {}
    for key in timetaken.keys():
        if key not in attributes:
            attributes[key] = (np.mean(timetaken[key]), np.std(timetaken[key]))

    prev_trans = None
    keep_label = False
    trans_label = None
    # run simulation
    while semantics.enabled_transitions(net, markings):
        all_trans = list(semantics.enabled_transitions(net, markings))
        if len(all_trans) == 1:
            # skip selection if only one is available
            trans = all_trans[0]
        else:
            if prev_trans is None:
                # First Event - choose random (usually only one trans)
                trans = np.random.choice(all_trans)
            else:
                # Choose next event with weighted random
                if not keep_label or trans_label is None:
                    trans_label = get_next_event_weighted(
                        event_change_stats, prev_trans
                    )
                else:
                    keep_label = False
                # get transition with the choosen label
                trans = [trans for trans in all_trans if trans.label == trans_label]
                if len(trans) != 1:
                    # It was not found - we are at a tauSplit or skip
                    trans = get_trans_leading_to_label(
                        all_trans, trans_label, net, markings
                    )
                    keep_label = True
                    if trans is None:
                        logger.debug(
                            "No nested trans was found for label " + trans_label
                        )
                        trans = np.random.choice(all_trans)
                        keep_label = False
                    else:
                        logger.debug("Found nested trans")
                else:
                    trans = trans[0]
        if trans.label is not None:
            prev_trans = trans.label

        markings = semantics.execute(trans, net, markings)
        if trans.label in attributes:
            # Calculate duration for the event with mean value and standard deviation. Needs to be positive.
            timeout = min(
                max(
                    np.random.normal(
                        attributes[trans.label][0],
                        attributes[trans.label][1],
                    ),
                    0,
                ),
                2 * attributes[trans.label][0],
            )
            if env.now is not None and not math.isnan(env.now):
                eLogDF.add(
                    Series(
                        {
                            "concept:name": trans.label,
                            "case:concept:name": f"Case {id}",
                            "time:timestamp": start_time
                            + datetime.timedelta(seconds=env.now),
                            "time:complete": start_time
                            + datetime.timedelta(seconds=env.now)
                            + datetime.timedelta(seconds=timeout),
                        }
                    )
                )
            yield env.timeout(timeout)


def get_trans_leading_to_label(
    all_trans, trans_label, net: PetriNet, markings: Marking, past_trans=[]
) -> Transition | None:
    """
    Find the unnamed transition leading to the choosen transition
    :param all_trans: all enabled transitions
    :type all_trans:
    :param trans_label: The label of the transition to find
    :type trans_label:
    :param net: The petri net
    :type net: PetriNet
    :param markings: the current marking of the petri net
    :type markings: Marking
    :param past_trans: All already fired transitions
    :type past_trans:
    :return: The transition leading to the choosen transition or None if it was not found
    :rtype: Transition | None
    """
    logger.debug("Find nested trans for label " + trans_label)
    # Get all trans with a None label
    none_trans = [x for x in all_trans if x.label is None]
    for trans in none_trans:
        if trans in past_trans:
            # loop detection
            continue
        # Execute found trans
        logger.debug(trans)
        if trans in semantics.enabled_transitions(net, markings):
            new_markings = semantics.execute(trans, net, markings)
            logger.debug("executed trans")
            past_trans.append(trans)
        else:
            logger.debug("NOT ENABLED")
            continue
        logger.debug(new_markings)
        if new_markings is None:
            continue
        # Get all trans labels
        all_new_trans = list(semantics.enabled_transitions(net, new_markings))
        # Check if choosen label was found, if yes return
        new_trans = [trans for trans in all_new_trans if trans.label == trans_label]
        logger.debug("New Trans ", new_trans)
        if len(new_trans) == 1:
            return trans
        else:
            nested_trans = get_trans_leading_to_label(
                all_new_trans, trans_label, net, new_markings, past_trans
            )
            if nested_trans is not None:
                return trans
    return None


def save_event_change_stats_to_db(
    event_change_stats, sim_run: SimulationRun, simulated: bool
):
    """
    Save the event change stats to the database
    :param event_change_stats:
    :type event_change_stats:
    :param sim_run: The assosiated simulation run
    :type sim_run: SimulationRun
    :param simulated: Indicates if the data is simulated or from the original event log
    :type simulated: bool
    """
    for key in event_change_stats.keys():
        if simulated:
            stat_obj = EventChangeStat.objects.get(simulation_run=sim_run, name=key)
        else:
            stat_obj = EventChangeStat(simulation_run=sim_run, name=key)
            stat_obj.save()
        for metric in event_change_stats[key]:
            try:
                metric_obj = EventChangeStatMetric.objects.get(
                    event_change_stat=stat_obj, name=metric
                )
            except ObjectDoesNotExist:
                metric_obj = EventChangeStatMetric(
                    event_change_stat=stat_obj, name=metric
                )
            if simulated:
                metric_obj.value_s = event_change_stats[key][metric]
            else:
                metric_obj.value_o = event_change_stats[key][metric]
            metric_obj.save()
