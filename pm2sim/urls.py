#   Copyright © 2024 Tobias Mieves
#  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#  THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
from django.conf.urls.static import static
from django.urls import path

from Artefakt import settings
from pm2sim import views

urlpatterns = [
    path("", views.home_view, name="home_view"),
    path("eventLogs", views.EventLogListView.as_view(), name="eventlogList"),
    path(
        "eventLogs/new", views.EditOrCreateEventLogView.as_view(), name="createEventLog"
    ),
    path(
        "eventLogs/<uuid:pk>", views.EventLogDetailView.as_view(), name="eventlogDetail"
    ),
    path(
        "eventLogs/<uuid:pk>/delete",
        views.DeleteEventLogView.as_view(),
        name="deleteEventLog",
    ),
    path(
        "eventLogs/<uuid:pk>/analyse/original",
        views.generate_event_log_data,
        name="analyseEventLog",
    ),
    path("eventLogs/<uuid:pk>/simulate", views.simulate, name="simulateEventLog"),
    path(
        "eventLogs/<uuid:event_log_pk>/analyse/simulated/<int:simulation_run_id>",
        views.analyse_simulated_event_log,
        name="analyseSimulatedEventLog",
    ),
    path(
        "simulationRun/<int:pk>/delete",
        views.DeleteSimulationRunView.as_view(),
        name="deleteSimulationRun",
    ),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
