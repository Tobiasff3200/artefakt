#   Copyright © 2024 Tobias Mieves
#  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#  THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import datetime
import logging
import uuid

import pandas as pd
import pm4py
import simpy
from django.core.exceptions import ObjectDoesNotExist
from django.core.files import File
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse, reverse_lazy
from django.utils import timezone
from django.views.generic import CreateView, DeleteView, DetailView, ListView
from pandas import DataFrame
from pm4py import fitness_token_based_replay

from pm2sim import util
from pm2sim.models import EvaluationResults, EventLog, SimulationRun
from pm2sim.util import EventLogDataFrame

logger = logging.getLogger(__name__)


def home_view(request):
    return HttpResponseRedirect(reverse("eventlogList"))


class EventLogListView(ListView):
    model = EventLog


class EditOrCreateEventLogView(CreateView):
    model = EventLog
    fields = ["name", "file"]


class DeleteEventLogView(DeleteView):
    model = EventLog
    success_url = reverse_lazy("eventlogList")


class EventLogDetailView(DetailView):
    model = EventLog


class DeleteSimulationRunView(DeleteView):
    model = SimulationRun
    success_url = reverse_lazy("eventlogList")


def generate_event_log_data(request, pk):
    """
    Calculates start and end activities, amount events and generates bpmn image of original event log.
    :param request:
    :type request:
    :param pk: ID of the event log
    :type pk: uuid
    :return:
    :rtype:
    """
    logger.info("Generating event log data.")
    obj: EventLog = get_object_or_404(EventLog, id=pk)
    event_log = util.read_event_log(obj.file.path)
    logger.debug("Get start and end activities")
    start_activities = pm4py.get_start_activities(event_log)
    end_activities = pm4py.get_end_activities(event_log)
    obj.start_activities = str(list(start_activities.keys()))
    obj.end_activities = str(list(end_activities.keys()))
    logger.debug("Calculating total number of cases")
    obj.number_of_cases = len(event_log["case:concept:name"].unique())

    path = util.generate_bpmn_process_img(event_log, obj.id)
    with open(path, "rb") as f:
        file = File(f)
        obj.process_img.save(str(obj.id) + ".png", file)
        obj.save()

    obj.save()
    return HttpResponseRedirect(reverse("eventlogDetail", kwargs={"pk": obj.id}))


def simulate(request, pk):
    """
    Calculates the statistical data of the original event log, simulates it and calculates the statistical data of
    the simulated log.
    :param request:
    :type request:
    :param pk: id of the event log
    :type pk: uuid
    :return:
    :rtype:
    """
    logger.info("Starting simulation")
    obj: EventLog = get_object_or_404(EventLog, id=pk)
    logger.debug("Create new simulation run")
    sim_run = SimulationRun()
    sim_run.event_log = obj
    sim_run.save()
    event_log: DataFrame = util.read_event_log(obj.file.path)
    logger.debug("Discover petri net")
    net, start_markings, end_markings = pm4py.discover_petri_net_inductive(
        event_log,
        activity_key="concept:name",
        case_id_key="case:concept:name",
        timestamp_key="time:timestamp",
    )
    # calculate statistical data for original event log
    timetaken = util.get_concept_times(event_log)
    util.save_time_to_db(timetaken, sim_run, simulated=False)
    start_times = util.get_process_start_time_stats(event_log)
    sim_run.start_time_mean_o = start_times[0]
    sim_run.start_time_std_o = start_times[1]
    sim_run.save()
    event_change_stats = util.get_event_change_stats(event_log)
    util.save_event_change_stats_to_db(event_change_stats, sim_run, simulated=False)

    logger.debug("Setting up simulation environment")
    num_cases = 1000
    evLogDF = EventLogDataFrame()

    env = simpy.Environment()
    starttime = pd.Timestamp(datetime.datetime.now())

    env.process(
        util.generate_simulation_processes(
            env,
            net,
            start_markings,
            timetaken,
            start_times,
            event_change_stats,
            evLogDF,
            starttime,
            num_cases,
        )
    )
    logger.debug("Run simulation")
    env.run()
    logger.debug("Finished simulation")

    logger.debug("Save generated event log")
    path = f"/tmp/{obj.id}-generated-log.csv"
    evLogDF.event_log.to_csv(path)
    with open(path, "rb") as f:
        file = File(f)
        sim_run.simulated_event_log.save(str(obj.id) + "-generated-log.csv", file)
        sim_run.save()

    logger.debug("Save process image")
    sim_log = util.read_event_log(
        path, "case:concept:name", "concept:name", "time:timestamp"
    )
    # Calculate statistical data for simulated log
    timetaken_s = util.get_concept_times(sim_log)
    util.save_time_to_db(timetaken_s, sim_run, simulated=True)
    start_times_s = util.get_process_start_time_stats(sim_log)
    sim_run.start_time_mean_s = start_times_s[0]
    sim_run.start_time_std_s = start_times_s[1]
    sim_run.save()
    event_change_stats_s = util.get_event_change_stats(sim_log)
    util.save_event_change_stats_to_db(event_change_stats_s, sim_run, simulated=True)

    # Generate process image
    sim_img_path = util.generate_bpmn_process_img(sim_log, obj.id)
    with open(sim_img_path, "rb") as f:
        file = File(f)
        sim_run.simulated_process_img.save(str(obj.id) + ".png", file)
        sim_run.save()

        print(start_times)
        print(start_times_s)
    # save duration of the simulation run
    now = timezone.make_aware(datetime.datetime.now())
    sim_run.duration = (now - sim_run.timestamp).total_seconds()
    sim_run.save()
    return HttpResponseRedirect(reverse("eventlogDetail", kwargs={"pk": obj.id}))


def analyse_simulated_event_log(request, event_log_pk: uuid, simulation_run_id: int):
    """
    Run token replay and alignment analysis
    :param request:
    :type request:
    :param event_log_pk: ID of the event log
    :type event_log_pk: uuid
    :param simulation_run_id: ID of the simulation run
    :type simulation_run_id: int
    :return:
    :rtype:
    """
    obj: EventLog = get_object_or_404(EventLog, id=event_log_pk)
    sim_run: SimulationRun = get_object_or_404(SimulationRun, id=simulation_run_id)
    event_log: DataFrame = util.read_event_log(obj.file.path)
    simulated_event_log: DataFrame = util.read_event_log(
        sim_run.simulated_event_log.path,
        case_id="case:concept:name",
        activity_key="concept:name",
        timestamp_key="time:timestamp",
    )
    net_o, start_markings_o, end_markings_o = pm4py.discover_petri_net_inductive(
        event_log
    )
    net_s, start_markings_s, end_markings_s = pm4py.discover_petri_net_inductive(
        simulated_event_log
    )

    try:
        results = EvaluationResults.objects.get(simulation_run=sim_run)
    except ObjectDoesNotExist:
        results = EvaluationResults(simulation_run=sim_run)

    tr_fitness = fitness_token_based_replay(
        simulated_event_log, net_o, start_markings_o, end_markings_o
    )
    results.tr_sim_log_on_orig_mod_av_trace_fitness = tr_fitness[
        "average_trace_fitness"
    ]
    results.tr_sim_log_on_orig_mod_perc_fit_traces = tr_fitness["perc_fit_traces"]
    results.save()  # Save after each step because some may take forever

    al_fitness = pm4py.fitness_alignments(
        simulated_event_log, net_o, start_markings_o, end_markings_o
    )
    results.al_sim_log_on_orig_mod_av_trace_fitness = al_fitness[
        "average_trace_fitness"
    ]
    results.al_sim_log_on_orig_mod_perc_fit_traces = al_fitness["percFitTraces"]
    results.save()

    tr_fitness = pm4py.fitness_token_based_replay(
        event_log, net_s, start_markings_s, end_markings_s
    )
    results.tr_orig_log_on_sim_mod_perc_fit_traces = tr_fitness["perc_fit_traces"]
    results.tr_orig_log_on_sim_mod_av_trace_fitness = tr_fitness[
        "average_trace_fitness"
    ]
    results.save()

    al_fitness = pm4py.fitness_alignments(
        event_log, net_s, start_markings_s, end_markings_s
    )
    results.al_orig_log_on_sim_mod_perc_fit_traces = al_fitness["percFitTraces"]
    results.al_orig_log_on_sim_mod_av_trace_fitness = al_fitness[
        "average_trace_fitness"
    ]
    results.save()

    results.al_sim_log_on_orig_mod_precision = pm4py.precision_alignments(
        simulated_event_log, net_o, start_markings_o, end_markings_o
    )
    results.save()
    results.tr_sim_log_on_orig_mod_precision = pm4py.precision_token_based_replay(
        simulated_event_log, net_o, start_markings_o, end_markings_o
    )
    results.save()
    results.tr_orig_log_on_sim_mod_precision = pm4py.precision_token_based_replay(
        event_log, net_s, start_markings_s, end_markings_s
    )
    results.save()

    results.al_orig_log_on_sim_mod_precision = pm4py.precision_alignments(
        event_log, net_s, start_markings_s, end_markings_s
    )
    results.save()
    return HttpResponseRedirect(reverse("eventlogDetail", kwargs={"pk": event_log_pk}))
