#   Copyright © 2024 Tobias Mieves
#  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#  THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import uuid

from django.core.validators import FileExtensionValidator
from django.db import models
from django.urls import reverse


class EventLog(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=500)
    file = models.FileField(
        upload_to="event-logs/",
        validators=[FileExtensionValidator(allowed_extensions=["xes", "csv"])],
    )
    start_activities = models.CharField(max_length=5000, null=True, blank=True)
    end_activities = models.CharField(max_length=5000, null=True, blank=True)
    number_of_cases = models.IntegerField(null=True, blank=True)
    process_img = models.ImageField(upload_to="process_images/", null=True, blank=True)

    def get_absolute_url(self):
        return reverse("eventlogDetail", kwargs={"pk": self.id})


class SimulationRun(models.Model):
    event_log = models.ForeignKey(EventLog, on_delete=models.CASCADE)
    start_time_mean_o = models.FloatField(null=True, blank=True)
    start_time_std_o = models.FloatField(null=True, blank=True)
    start_time_mean_s = models.FloatField(null=True, blank=True)
    start_time_std_s = models.FloatField(null=True, blank=True)
    simulated_process_img = models.ImageField(
        upload_to="simulated_process_images/", null=True, blank=True
    )
    simulated_event_log = models.FileField(
        upload_to="generated_logs/", null=True, blank=True
    )
    timestamp = models.DateTimeField(auto_now_add=True)
    duration = models.IntegerField(null=True, blank=True)

    class Meta:
        ordering = ("-timestamp",)


class EventTime(models.Model):
    name = models.CharField(max_length=5000)
    mean_time_o = models.FloatField(null=True, blank=True)
    std_deviation_o = models.FloatField(null=True, blank=True)
    mean_time_s = models.FloatField(null=True, blank=True)
    std_deviation_s = models.FloatField(null=True, blank=True)
    simulation_run = models.ForeignKey(
        to=SimulationRun, related_name="event_times", on_delete=models.CASCADE
    )


class EvaluationResults(models.Model):
    simulation_run = models.ForeignKey(
        to=SimulationRun, related_name="evaluation_results", on_delete=models.CASCADE
    )
    tr_sim_log_on_orig_mod_precision = models.FloatField(null=True, blank=True)
    tr_sim_log_on_orig_mod_perc_fit_traces = models.FloatField(null=True, blank=True)
    tr_sim_log_on_orig_mod_av_trace_fitness = models.FloatField(null=True, blank=True)
    tr_orig_log_on_sim_mod_precision = models.FloatField(null=True, blank=True)
    tr_orig_log_on_sim_mod_perc_fit_traces = models.FloatField(null=True, blank=True)
    tr_orig_log_on_sim_mod_av_trace_fitness = models.FloatField(null=True, blank=True)

    al_sim_log_on_orig_mod_precision = models.FloatField(null=True, blank=True)
    al_sim_log_on_orig_mod_perc_fit_traces = models.FloatField(null=True, blank=True)
    al_sim_log_on_orig_mod_av_trace_fitness = models.FloatField(null=True, blank=True)
    al_orig_log_on_sim_mod_precision = models.FloatField(null=True, blank=True)
    al_orig_log_on_sim_mod_perc_fit_traces = models.FloatField(null=True, blank=True)
    al_orig_log_on_sim_mod_av_trace_fitness = models.FloatField(null=True, blank=True)


class EventChangeStat(models.Model):
    simulation_run = models.ForeignKey(SimulationRun, on_delete=models.CASCADE)
    name = models.CharField(max_length=5000)


class EventChangeStatMetric(models.Model):
    event_change_stat = models.ForeignKey(EventChangeStat, on_delete=models.CASCADE)
    name = models.CharField(max_length=5000)
    value_o = models.FloatField(null=True, blank=True)
    value_s = models.FloatField(null=True, blank=True)
