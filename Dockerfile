FROM python:3.12 as base
WORKDIR /app
COPY requirements.txt requirements.txt
RUN apt-get update && apt-get install -y --no-install-recommends graphviz && rm -rf /var/lib/apt/lists/*
RUN pip install --no-cache-dir -r requirements.txt && rm requirements.txt

FROM node:21 AS node
WORKDIR /app
COPY . /app
RUN npm ci && npm run tailwind

FROM base
COPY --from=node /app /app
RUN rm /app/static/css/main.css && mkdir -p /app/db/ && chmod +x /app/start.sh
EXPOSE 8000
ENTRYPOINT ["./start.sh"]
