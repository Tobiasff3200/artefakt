# Artefakt

Dies ist das Artefakt, das im Rahmen meiner Bachelorarbeit entstanden ist. Es erlaubt es, aus Event-Logs automatisiert ein Simulationsmodell zu erstellen, das neue Event-Logs generiert.

## Starten des Artefakts

Das Artefakt kann mit Docker oder direkt mit Python ausgeführt werden.

### Docker (einfach)

Am einfachsten ist das Starten mit Docker compose.

Dafür muss das Repository heruntergeladen und dann mit `docker compose up` gestartet werden. Das Artefakt kann dann unter [http://localhost:8000](http://localhost:8000) erreicht werden.

### Nativ (aufwendiger)

Um das Artefakt ohne Docker zu starten, muss Python und NPM installiert sein.

Nach dem Herunterladen des Repositories, müssen die Abhängigkeiten mit `pip install -r requirements.txt` installiert werden.

Anschließend muss mit `npm install` und `npm run tailwind` das Stylesheet compiliert werden.

Mit `python manage.py migrate` und `python manage.py collectstatic` wird das Backend vorbereitet.

Anschließend kann es mit `python manage.py runserver` ausgeführt werden.

Das Artefakt kann dann unter [http://localhost:8000](http://localhost:8000) erreicht werden.